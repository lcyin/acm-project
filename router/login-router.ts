import * as passport from 'passport';
import * as express from 'express';
import { Request, Response, NextFunction } from 'express';
import * as passportLocal from 'passport-local';
import { User, Result } from "../interface";
import { authentication, findUserById } from '../service/user-service';

const LocalStrategy = passportLocal.Strategy;
passport.use("local", new LocalStrategy(
    async (username, password, done) => {
        try {
            const login: User = { Username: username, Password: password };
            const result: Result = await authentication(login);
            //login ok
            if (result.result) {
                return done(null, result.loginUser);
            }
            //login fail
            else 
                if (result.error) {
                    return done(null, false, { message: result.error });
                }
        }
        catch (err) {
            return done(err);
        }
    })
)

//use the id to find the user
passport.serializeUser((user: User, done) => {
    done(null, user.id);
});

passport.deserializeUser(async (id: number, done) => {
    try {
        const result: Result = await findUserById(id);
        //user found
        if (result.result) {
            done(null, result.loginUser);
        }
        //user not found
        else {
            done(result.error);
        }
    }
    catch (err) {
        done(err);
    }
});


const login = (req: Request, res: Response) => {
    passport.authenticate("local", (err: Error, user: Object, info: string) => {
        try {
            if (err) {
                res.json(err);
                return;
            }

            //login fail
            else if (!user) {
                const result: Result = { result: false, error: info["message"] };
                res.status(401).json(result);
                return;
            }

            //login ok
            else {
                req.login(user, (err) => {
                    if (err) {
                        res.json(err);
                        return;
                    }
                    else {
                        const result: Result = { result: true };
                        res.json(result);
                    }
                })
            }
        } catch (err) { res.json(err); }
    })(req, res)
}

const logout = (req: Request, res: Response) => {
    try {
        req.logOut();
        const result: Result = { result: true };
        res.json(result);
    }
    catch (err) { res.json(err); }
}

//check if the user is login
export const checkLogin = (req: Request, res: Response, next: NextFunction) => {
    //is login
    if (req.user) {
        next();
    }
    //do not login
    else {
        const result: Result = { result: false, error: "You do not login" };
        res.status(401).json(result);
        return;
    }
}

export class loginRouter {

    router() {
        const router = express.Router();
        router.post("/", login);
        router.post("/logout", logout);
        return router;
    }
}



