import * as express from "express";
import { Request, Response } from "express";
import { Result, AirConditioner } from "../interface";
import { createAirConditioner } from "../service/air_conditioner-service";
import { findIdByName } from "../service/client-service";

const create = async (req: Request, res: Response) => {
    try {
        //turn the client name to the database id
        const resultClientId: Result = await findIdByName(req.body.clientName);
        //the name is not found in the client database
        if (!resultClientId.result) {
            res.json(resultClientId);
            return;
        }
        //insert the data into the database
        else {
            if (resultClientId.id) {
                const clientId = resultClientId.id;
                const newAirConditioner: AirConditioner = {
                    Air_Conditioner_Id: req.body.air_conditioner_id,
                    Model_No: req.body.model_no,
                    User_Id: req.user.id,
                    Client_Id: clientId,
                    Location: req.body.location
                }
                const result: Result = await createAirConditioner(newAirConditioner);
                res.json(result);
            }
        }
    }
    catch (err) { res.json(err); }
}

export class airConditionerRouter {
    router() {
        const router = express.Router();
        router.post("/", create);
        return router;
    }
}