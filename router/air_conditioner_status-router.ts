import * as express from "express";
import { Request, Response } from "express";
import { Result, AirConditionerStatus } from "../interface";
import { createAirConditionerStatus, getAllAirConditionerDataByClient } from "../service/air_conditioner_status-service";

const create = async (req: Request, res: Response) => {
    try {

        const newAirConditionerStatus: AirConditionerStatus = {
            Air_Conditioner_Id: req.body.id,
            Current_Temperature: req.body.temperature,
            Humidity: req.body.humidity,
            DateTime: req.body.timestamp,
            Expected_Temperature: req.body.expectedTemperature
        }

        const result: Result = await createAirConditionerStatus(newAirConditionerStatus);
        res.json(result);
    }
    catch (err) { res.json(err); }
}

const getAll = async (req: Request,res:Response)=>{
    try{
        const result: Result = await getAllAirConditionerDataByClient(req.query.name);
        res.json(result);
    }
    catch(err){res.json(err);}
}


export class airConditionerStatusRouter {
    router() {
        const router = express.Router();
        router.post("/", create);
        router.get("/getall",getAll);
        return router;
    }
}