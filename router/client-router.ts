import * as express from "express";
import { Request, Response } from "express";
import { createClient, getAllClientName, updateClient } from "../service/client-service";
import { Client, ClientName, UpdateClient, Result } from "../interface";

const create = async (req: Request, res: Response) => {
    try {
        const newClient: Client = {
            Name: req.body.name,
            Telephone: req.body.telephone,
            Email: req.body.email,
            Address: req.body.address
        }
        const result: Result = await createClient(newClient);
        res.json(result);
    }
    catch (err) { res.json(err); }
}

const getName = async (req: Request, res: Response) => {
    try {
        const result: ClientName[] = await getAllClientName();
        res.json(result);
    }
    catch (err) { res.json(err); }
}

const update = async (req: Request, res: Response) => {
    try {
        const aupdateClient: UpdateClient = {
            OriginalName: req.body.originalname,
            UpdatedName: req.body.updatedname,
            Telephone: req.body.telephone,
            Email: req.body.email,
            Address: req.body.address
        };
        const result: Result = await updateClient(aupdateClient);
        res.json(result);
    }
    catch (err) { res.json(err); }
}

export class clientRouter {
    router() {
        const router = express.Router();
        router.get("/get-name", getName);
        router.post("/", create);
        router.put("/", update);
        return router;
    }
}