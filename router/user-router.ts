import * as express from "express";
import { Request, Response } from "express";
import { createUser, changePassword, authentication } from "../service/user-service";
import { User, Result } from "../interface";

const create = async (req: Request, res: Response) => {
    try {
        const newUser: User = {
            Username: req.body.username,
            Password: req.body.password
        }
        const result: Result = await createUser(newUser);
        res.json(result);
    }
    catch (err) { res.json(err); }
}

const update = async (req: Request, res: Response) => {
    try {
        //check the orginal password is correct for the user
        const checkPassword : User = {
            Username: req.user.Username,
            Password: req.body.originalPassword
        }
        const result: Result = await authentication(checkPassword);
        //the orginal password is not correct
        if(!result.result){
            res.json(result);
            return;
        }
        //then update the user password
        else{
            const updateUser: User = {
                Username: req.user.Username,
                Password: req.body.newPassword
            }
            const result: Result = await changePassword(updateUser);
            res.json(result);
        }
    }
    catch (err) { res.json(err); }
}


export class userRouter {
    router() {
        const router = express.Router();
        router.post("/", create);
        router.put("/",update);
        return router;
    }
}