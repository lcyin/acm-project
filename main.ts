import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as expressSession from 'express-session';
import * as passport from 'passport';
import { userRouter } from "./router/user-router";
import { loginRouter, checkLogin } from "./router/login-router";
import { clientRouter } from "./router/client-router";
import { airConditionerRouter } from "./router/air_conditioner-router";
import { airConditionerStatusRouter } from "./router/air_conditioner_status-router";

import { knex } from "./knex-config";
import {User,Client,AirConditioner, AirConditionerStatus} from "./interface";
import {createAirConditioner} from "./service/air_conditioner-service";
import {createClient} from "./service/client-service";
import {createUser} from "./service/user-service";
import { createAirConditionerStatus} from "./service/air_conditioner_status-service";

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(expressSession({
    secret: 'air_conditioner_monitor',
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false }
}));
app.use(passport.initialize());
app.use(passport.session());

const auserRouter = new userRouter();
app.use("/user", auserRouter.router());
const aloginRouter = new loginRouter();
app.use("/login", aloginRouter.router());
const aclientRouter = new clientRouter();
app.use("/client", aclientRouter.router());
const aairConditionerRouter = new airConditionerRouter();
app.use("/air-conditioner", aairConditionerRouter.router());
const aairConditionerStatusRouter =new airConditionerStatusRouter();
app.use("/air-conditioner-status", aairConditionerStatusRouter.router());
app.use(express.static('./public/user'))
app.use(checkLogin,express.static('./public/client'))



//testing initilise the data
async function initDatabase() {

    //drop all table value
    await knex("Air_Conditioner_Status").del();
    await knex("Air_Conditioner").del();
    await knex("User").del();
    await knex("Client").del();

    //make new user value
    const newUser: User = { Username: "hello", Password: "world" };
    await createUser(newUser);
    const user: User = await knex.select("*").from("User").where("Username", "hello");
    const userid = user[0].id;

    //make new client value
    const newClient: Client = {
        Name: "Hello Company",
        Telephone: "12345678",
        Email: "1234@gmail.com",
        Address: "Happy Street"
    };
    await createClient(newClient);
    const client: Client = await knex.select("*").from("Client").where("Name", "Hello Company");
    const clientid = client[0].id;

    //make new air conditioner value
    const newAirConditioner: AirConditioner = {
        Air_Conditioner_Id: "0001",
        Model_No: "AC01",
        User_Id: userid,
        Client_Id: clientid,
        Location: "Sad Building"
    };
    const newAirConditioner1: AirConditioner = {
        Air_Conditioner_Id: "0002",
        Model_No: "AC01",
        User_Id: userid,
        Client_Id: clientid,
        Location: "Sad Building"
    };
    
    await createAirConditioner(newAirConditioner);
    await createAirConditioner(newAirConditioner1);


    //make new air conditioner status value
    const newAirConditionerStatus: AirConditionerStatus = {
        Air_Conditioner_Id: "0001",
        Current_Temperature: 23,
        Humidity: 74,
        DateTime: "21/12/2018 10:00:00",
        Expected_Temperature: 22.0
    };
    const newAirConditionerStatus2: AirConditionerStatus = {
        Air_Conditioner_Id: "0001",
        Current_Temperature: 24,
        Humidity: 71,
        DateTime: "21/12/2018 10:15:00",
        Expected_Temperature: 21.5
    };
    const newAirConditionerStatus3: AirConditionerStatus = {
        Air_Conditioner_Id: "0001",
        Current_Temperature: 23,
        Humidity: 77,
        DateTime: "21/12/2018 10:30:00",
        Expected_Temperature: 25.5
    };
    const newAirConditionerStatus4: AirConditionerStatus = {
        Air_Conditioner_Id: "0001",
        Current_Temperature: 25,
        Humidity: 71,
        DateTime: "21/12/2018 10:45:00",
        Expected_Temperature: 24.0
    };
    const newAirConditionerStatus21: AirConditionerStatus = {
        Air_Conditioner_Id: "0001",
        Current_Temperature: 25,
        Humidity: 70,
        DateTime: "21/12/2018 11:00:00",
        Expected_Temperature: 21.0
    };
    const newAirConditionerStatus22: AirConditionerStatus = {
        Air_Conditioner_Id: "0001",
        Current_Temperature: 23,
        Humidity: 67,
        DateTime: "21/12/2018 11:15:00",
        Expected_Temperature: 23.6
    };
    const newAirConditionerStatus23: AirConditionerStatus = {
        Air_Conditioner_Id: "0002",
        Current_Temperature: 22,
        Humidity: 65,
        DateTime: "21/12/2018 11:30:00",
        Expected_Temperature: 24.6
    };


    await createAirConditionerStatus(newAirConditionerStatus);
    await createAirConditionerStatus(newAirConditionerStatus2);
    await createAirConditionerStatus(newAirConditionerStatus3);
    await createAirConditionerStatus(newAirConditionerStatus4);
    await createAirConditionerStatus(newAirConditionerStatus21); 
    await createAirConditionerStatus(newAirConditionerStatus22);
    await createAirConditionerStatus(newAirConditionerStatus23);
}


const PORT = 8080;
app.listen(PORT, async() => {
    await initDatabase();
    console.log(`Listening at http://localhost:${PORT}/`);
});
