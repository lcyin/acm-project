import { createClient, findClientById, getAllClientName, updateClient, findIdByName } from "../service/client-service";
import { knex } from "../knex-config";
import { Result, Client, ClientName, UpdateClient } from "../interface";

beforeEach(async () => {
    await knex("Air_Conditioner_Status").del();
    await knex("Air_Conditioner").del();
    await knex("User").del();
    await knex("Client").del();
})

describe("Create Client", async () => {

    it("Create a normal client", async () => {
        const newClient: Client = {
            Name: "Hello Company",
            Telephone: "12345678",
            Email: "1234@gmail.com",
            Address: "Happy Street"
        };
        const result: Result = await createClient(newClient);
        const resultClient: Client[] = await knex.select("*").from("Client").where("Name", "Hello Company");
        expect(result.result).toBeTruthy();
        expect(resultClient[0]).toMatchObject({
            id: resultClient[0].id,
            Name: "Hello Company",
            Telephone: "12345678",
            Email: "1234@gmail.com",
            Address: "Happy Street"
        })
    })

    it("Create a client with empty value", async () => {
        const newClient: Client = {
            Name: "",
            Telephone: "",
            Email: "",
            Address: ""
        };
        const result: Result = await createClient(newClient);
        expect(result.error).toBe("Client value cannot be empty");
    })

    it("Create a client with incorrect telephone format", async () => {
        const newClient: Client = {
            Name: "Hello Company",
            Telephone: "123abc78",
            Email: "1234@gmail.com",
            Address: "Happy Street"
        };
        const result: Result = await createClient(newClient);
        expect(result.error).toBe("The telephone number format is not correct");
    })

    it("Create a client with incorrect email format", async () => {
        const newClient: Client = {
            Name: "Hello Company",
            Telephone: "12345678",
            Email: "4321@gma@il@com",
            Address: "Happy Street"
        };
        const result: Result = await createClient(newClient);
        expect(result.error).toBe("The email format is not correct");
    })

    it("Create a client that client name is already exist", async () => {
        const newClient: Client = {
            Name: "Hello Company",
            Telephone: "12345678",
            Email: "1234@gmail.com",
            Address: "Happy Street"
        };
        await createClient(newClient);
        const result: Result = await createClient(newClient);
        expect(result.error).toBe("Client name already exist");
    })

})

describe("Find Client By Id", () => {
    let id: number;
    beforeEach(async () => {
        const newClient: Client = {
            Name: "Hello Company",
            Telephone: "12345678",
            Email: "1234@gmail.com",
            Address: "Happy Street"
        };
        await createClient(newClient);
        const client: Client[] = await knex.select("*").from("Client").where("Name", "Hello Company");
        if(client[0].id){
            id = client[0].id;
        }
    })

    it("Find the Hello Company client successfully by id", async () => {
        const result: Result = await findClientById(id);
        expect(result.result).toBeTruthy();
        if (result.client) {
            expect(result.client).toMatchObject({
                id: result.client.id,
                Name: "Hello Company",
                Telephone: "12345678",
                Email: "1234@gmail.com",
                Address: "Happy Street"
            })
        }
    })

    it("Cannot find the Hello Company client with the wrong id", async () => {
        const result: Result = await findClientById(id - 10);
        expect(result.result).toBeFalsy();
    })
})

describe("Find Client By Name", () => {
    let id: number;
    beforeEach(async () => {
        const newClient: Client = {
            Name: "Hello Company",
            Telephone: "12345678",
            Email: "1234@gmail.com",
            Address: "Happy Street"
        };
        await createClient(newClient);
        const client: Client[] = await knex.select("*").from("Client").where("Name", "Hello Company");
        if(client[0].id){
            id = client[0].id;
        }
    })

    it("Find the Hello Company client successfully by name", async () => {
        const result: Result = await findIdByName("Hello Company");
        expect(result.result).toBeTruthy();
        expect(result.id).toBe(id);
    })

    it("Cannot find the Hello Company client with the wrong name", async () => {
        const result: Result = await findIdByName("Helloa Company");
        expect(result.result).toBeFalsy();
    })
})

describe("Get All The Client Name", async () => {

    it("Get all three client name", async () => {
        let newClient: Client;
        let id: number[] = new Array();
        for (let i = 0; i < 3; i++) {
            newClient = {
                Name: "Hello Company" + i,
                Telephone: "12345678",
                Email: "1234@gmail.com",
                Address: "Happy Street"
            };
            await createClient(newClient);
            const client: Client[] = await knex.select("*").from("Client").where("Name", "Hello Company" + i);
            if(client[0].id){
                id[i] = client[0].id;
            }
        }
        const result: ClientName[] = await getAllClientName();
        expect(result).toMatchObject([
            {
                id: id[0],
                Name: "Hello Company0"
            },
            {
                id: id[1],
                Name: "Hello Company1"
            },
            {
                id: id[2],
                Name: "Hello Company2"
            }
        ]);
    })

    it("Get none of the value in a empty client database", async () => {
        const result: ClientName[] = await getAllClientName();
        expect(result).toMatchObject([]);
    })

})

describe("Update The Client Information", async () => {

    beforeEach(async () => {
        const newClient1 = {
            Name: "Hello Company",
            Telephone: "12345678",
            Email: "1234@gmail.com",
            Address: "Happy Street"
        };
        const newClient2 = {
            Name: "Happy Company",
            Telephone: "12345678",
            Email: "1234@gmail.com",
            Address: "Happy Street"
        };
        await createClient(newClient1);
        await createClient(newClient2);
    })

    it("Update only the name", async () => {
        const aupdateClient: UpdateClient = {
            OriginalName: "Hello Company",
            UpdatedName: "Good Company"
        };
        await updateClient(aupdateClient);
        const client: Client[] = await knex.select("*").from("Client").where("Name", "Good Company");
        expect(client[0]).toMatchObject({
            id: client[0].id,
            Name: "Good Company",
            Telephone: "12345678",
            Email: "1234@gmail.com",
            Address: "Happy Street"
        })
    })

    it("Using the original name after updating", async () => {
        const aupdateClient: UpdateClient = {
            OriginalName: "Hello Company",
            UpdatedName: "Good Company"
        };
        await updateClient(aupdateClient);
        const result: Result = await updateClient(aupdateClient);
        expect(result.error).toBe("Client not found");
    })

    it("Update only the name but the name is already exists", async () => {
        const aupdateClient: UpdateClient = {
            OriginalName: "Hello Company",
            UpdatedName: "Happy Company"
        };
        const result: Result = await updateClient(aupdateClient);
        expect(result.error).toBe("Update client name already exists")
    })

    it("Update only the telephone", async () => {
        const aupdateClient: UpdateClient = {
            OriginalName: "Hello Company",
            Telephone: "24125678"
        };
        await updateClient(aupdateClient);
        const client: Client[] = await knex.select("*").from("Client").where("Name", "Hello Company");
        expect(client[0]).toMatchObject({
            id: client[0].id,
            Name: "Hello Company",
            Telephone: "24125678",
            Email: "1234@gmail.com",
            Address: "Happy Street"
        })
    })

    it("Update only the email", async () => {
        const aupdateClient: UpdateClient = {
            OriginalName: "Hello Company",
            Email: "4321@gmail.com"
        };
        await updateClient(aupdateClient);
        const client: Client[] = await knex.select("*").from("Client").where("Name", "Hello Company");
        expect(client[0]).toMatchObject({
            id: client[0].id,
            Name: "Hello Company",
            Telephone: "12345678",
            Email: "4321@gmail.com",
            Address: "Happy Street"
        })
    })

    it("Update only the address", async () => {
        const aupdateClient: UpdateClient = {
            OriginalName: "Hello Company",
            Address: "Sad Street"
        };
        await updateClient(aupdateClient);
        const client: Client[] = await knex.select("*").from("Client").where("Name", "Hello Company");
        expect(client[0]).toMatchObject({
            id: client[0].id,
            Name: "Hello Company",
            Telephone: "12345678",
            Email: "1234@gmail.com",
            Address: "Sad Street"
        })
    })

    it("Update all the data", async () => {
        const aupdateClient: UpdateClient = {
            OriginalName: "Hello Company",
            UpdatedName: "Good Company",
            Telephone: "24125678",
            Email: "4321@gmail.com",
            Address: "Sad Street"
        };
        await updateClient(aupdateClient);
        const client: Client[] = await knex.select("*").from("Client").where("Name", "Good Company");
        expect(client[0]).toMatchObject({
            id: client[0].id,
            Name: "Good Company",
            Telephone: "24125678",
            Email: "4321@gmail.com",
            Address: "Sad Street"
        })
    })

    it("Update the data with empty value", async () => {
        const aupdateClient: UpdateClient = {
            OriginalName: "",
            UpdatedName: "",
            Telephone: "",
            Email: "",
            Address: ""
        }
        const result: Result = await updateClient(aupdateClient)
        expect(result.error).toBe("Client value cannot be empty");
    })

    it("Update the data with incorrect telephone format", async () => {
        const aupdateClient: UpdateClient = {
            OriginalName: "Hello Company",
            UpdatedName: "Good Company",
            Telephone: "2412abc5678",
            Email: "4321@gmail.com",
            Address: "Sad Street"
        }
        const result: Result = await updateClient(aupdateClient)
        expect(result.error).toBe("The telephone number format is not correct");
    })

    it("Update the data with incorrect email format", async () => {
        const aupdateClient: UpdateClient = {
            OriginalName: "Hello Company",
            UpdatedName: "Good Company",
            Telephone: "24125678",
            Email: "4321@gma@il@com",
            Address: "Sad Street"
        }
        const result: Result = await updateClient(aupdateClient)
        expect(result.error).toBe("The email format is not correct");
    })

})

afterAll(async () => {
    await knex.destroy();
})