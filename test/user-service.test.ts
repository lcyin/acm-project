import { createUser, authentication, findUserById, changePassword } from "../service/user-service";
import { knex } from "../knex-config";
import { User, Result } from "../interface";

beforeEach(async () => {
    await knex("Air_Conditioner_Status").del();
    await knex("Air_Conditioner").del();
    await knex("User").del();
    await knex("Client").del();
})

describe("Create User", () => {

    it("Create a normal user", async () => {
        const newUser: User = { Username: "hello", Password: "world" };
        const result: Result = await createUser(newUser);
        const resultUser: User[] = await knex.select("Username").from("User").where("Username", "hello");
        expect(result.result).toBeTruthy();
        expect(resultUser[0].Username).toBe("hello");
    })

    it("Create a user with empty username and password", async () => {
        const newUser: User = { Username: "", Password: "" };
        const result: Result = await createUser(newUser);
        expect(result.error).toBe("Username or password cannot be empty");
    })

    it("Create a user that username already exist", async () => {
        const newUser: User = { Username: "hello", Password: "world" };
        await createUser(newUser);
        const result: Result = await createUser(newUser);
        expect(result.error).toBe("Username already exist");
    })

})

describe("Login", () => {

    beforeEach(async () => {
        const newUser: User = { Username: "hello", Password: "world" };
        await createUser(newUser);
    })

    it("Login with correct username and password", async () => {
        const newUser: User = { Username: "hello", Password: "world" };
        const result: Result = await authentication(newUser);
        expect(result.result).toBeTruthy();
    })

    it("Login with incorrect username", async () => {
        const newUser: User = { Username: "helloa", Password: "world" };
        const result: Result = await authentication(newUser);
        expect(result.error).toBe("Username is not correct");
    })

    it("Login with incorrect password", async () => {
        const newUser: User = { Username: "hello", Password: "warld" };
        const result: Result = await authentication(newUser);
        expect(result.error).toBe("Password is not correct");
    })

    it("Login with empty username and password", async () => {
        const newUser: User = { Username: "", Password: "" };
        const result: Result = await authentication(newUser);
        expect(result.error).toBe("Username or password cannot be empty");
    })

})

describe("Find User By Id", () => {
    let id: number;
    beforeEach(async () => {
        const newUser: User = { Username: "hello", Password: "world" };
        await createUser(newUser);
        const user: User[] = await knex.select("*").from("User").where("Username", "hello");
        if(user[0].id){
            id = user[0].id;
        }
    })

    it("Find the hello user successfully by id", async () => {
        const result: Result = await findUserById(id);
        expect(result.result).toBeTruthy();
        if (result.loginUser) {
            expect(result.loginUser.Username).toBe("hello");
        }
    })

    it("Cannot find the hello user with the wrong id", async () => {
        const result: Result = await findUserById(id - 10);
        expect(result.result).toBeFalsy();
    })
})

describe("Update User Password", () => {
    beforeEach(async () => {
        const newUser: User = { Username: "hello", Password: "world" };
        await createUser(newUser);
    })

    it("Update the user successfully", async () => {
        const updateUser: User = {
            Username: "hello",
            Password: "abcd"
        }
        await changePassword(updateUser);
        const result: Result = await authentication(updateUser);
        expect(result.result).toBeTruthy();
    })

    it("The user data is empty", async () => {
        const updateUser: User = {
            Username: "",
            Password: ""
        }
        const result: Result = await changePassword(updateUser);
        expect(result.error).toBe("Username or password cannot be empty");
    })

    it("The username does not find", async () => {
        const updateUser: User = {
            Username: "hella",
            Password: "abcd"
        }
        const result: Result = await changePassword(updateUser);
        expect(result.error).toBe("Username does not exist");
    })
})

afterAll(async () => {
    await knex.destroy();
})