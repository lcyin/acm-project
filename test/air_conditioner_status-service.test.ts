import { createUser } from "../service/user-service";
import { createClient } from "../service/client-service"
import { knex } from "../knex-config";
import { AirConditioner, User, Client, Result, AirConditionerStatus, databaseId } from "../interface";
import { createAirConditioner } from "../service/air_conditioner-service";
import {
    createDateFormatChange, getDateFormatChange, createAirConditionerStatus,
    getAirConditionerStatus, getAllAirConditionerDataById, getAllAirConditionerDataByClient
}
    from "../service/air_conditioner_status-service";


describe("Change Date Format In Create Function", () => {
    it("change 18/01/1997 10:12:13", () => {
        const dateTime = createDateFormatChange("18/01/1997 10:12:13");
        expect(dateTime).toBe("1997-01-18 10:12:13");
    })
})

describe("Change Date Format In Get Function", () => {
    it("change 2018-04-06T00:00:00Z", () => {
        const d = new Date("2018-04-06T00:00:00Z");
        const dateTime = getDateFormatChange(d);
        (d.getTimezoneOffset() != 0) ?
            expect(dateTime).toBe("2018-04-06 08:00:00"):
            expect(dateTime).toBe("2018-04-06 00:00:00")
            
    })
    it("change 2018-12-25T10:20:30Z", () => {
        const d = new Date("2018-12-25T10:20:30Z");
        const dateTime = getDateFormatChange(d);
        (d.getTimezoneOffset() != 0) ?
            expect(dateTime).toBe("2018-12-25 18:20:30") :
            expect(dateTime).toBe("2018-12-25 10:20:30")
    })
})

let userid: number;
let clientid: number;

beforeEach(async () => {
    //drop all table value
    await knex("Air_Conditioner_Status").del();
    await knex("Air_Conditioner").del();
    await knex("User").del();
    await knex("Client").del();

    //make new user value
    const newUser: User = { Username: "hello", Password: "world" };
    await createUser(newUser);
    const user: User[] = await knex.select("*").from("User").where("Username", "hello");
    if (user[0].id) {
        userid = user[0].id;
    }

    //make new client value
    const newClient: Client = {
        Name: "Hello Company",
        Telephone: "12345678",
        Email: "1234@gmail.com",
        Address: "Happy Street"
    };
    await createClient(newClient);
    const client: Client[] = await knex.select("*").from("Client").where("Name", "Hello Company");
    if (client[0].id) {
        clientid = client[0].id;
    }

    //make new air conditioner value
    const newAirConditioner1: AirConditioner = {
        Air_Conditioner_Id: "0001",
        Model_No: "AC01",
        User_Id: userid,
        Client_Id: clientid,
        Location: "Sad Building"
    };
    const newAirConditioner2: AirConditioner = {
        Air_Conditioner_Id: "0002",
        Model_No: "AC02",
        User_Id: userid,
        Client_Id: clientid,
        Location: "Happy Building"
    };
    await createAirConditioner(newAirConditioner1);
    await createAirConditioner(newAirConditioner2);
})

describe("Create Air Conditioner Status", () => {

    it("Create a normal air conditioner status", async () => {
        const newAirConditionerStatus: AirConditionerStatus = {
            Air_Conditioner_Id: "0001",
            Current_Temperature: 23,
            Humidity: 74,
            DateTime: "21/12/2018 10:57:45",
            Expected_Temperature: 20.0
        };
        const hour = (new Date().getTimezoneOffset() != 0) ? "02" : "10";
        const result: Result = await createAirConditionerStatus(newAirConditionerStatus);
        const indexIdArray: databaseId[] = await knex.select("id").from("Air_Conditioner")
            .where("Air_Conditioner_Id", "0001");
        const resultStatus: AirConditionerStatus[] = await knex.select("*").from("Air_Conditioner_Status")
            .where("Air_Conditioner_Id", indexIdArray[0].id);
        expect(result.result).toBeTruthy();
        expect(resultStatus[0]).toMatchObject({
            id: resultStatus[0].id,
            Air_Conditioner_Id: indexIdArray[0].id,
            Current_Temperature: 23,
            Expected_Temperature: 20.0,
            Humidity: 74,
            DateTime: new Date(`2018-12-21T${hour}:57:45Z`)
        })
    })

    it("Create an air conditioner status with lower than 0 degree", async () => {
        const newAirConditionerStatus: AirConditionerStatus = {
            Air_Conditioner_Id: "0001",
            Current_Temperature: -10,
            Humidity: 74,
            DateTime: "21/12/2018 10:57:45",
            Expected_Temperature: 20.0
        };
        const result: Result = await createAirConditionerStatus(newAirConditionerStatus);
        expect(result.error).toBe("The current temperature is lower than 0 degree");
    })

    it("Create an air conditioner status with lower than 0 humidity", async () => {
        const newAirConditionerStatus: AirConditionerStatus = {
            Air_Conditioner_Id: "0001",
            Current_Temperature: 23,
            Humidity: -5,
            DateTime: "21/12/2018 10:57:45",
            Expected_Temperature: 20.0
        };
        const result: Result = await createAirConditionerStatus(newAirConditionerStatus);
        expect(result.error).toBe("The humidity should be 0-100");
    })

    it("Create an air conditioner status with higher than 100 humidity", async () => {
        const newAirConditionerStatus: AirConditionerStatus = {
            Air_Conditioner_Id: "0001",
            Current_Temperature: 23,
            Humidity: 150,
            DateTime: "21/12/2018 10:57:45",
            Expected_Temperature: 20.0
        };
        const result: Result = await createAirConditionerStatus(newAirConditionerStatus);
        expect(result.error).toBe("The humidity should be 0-100");
    })

    it("Create an air conditioner status with incorrect date time format", async () => {
        const newAirConditionerStatus: AirConditionerStatus = {
            Air_Conditioner_Id: "0001",
            Current_Temperature: 23,
            Humidity: 78,
            DateTime: "21122018105745",
            Expected_Temperature: 20.0
        };
        const result: Result = await createAirConditionerStatus(newAirConditionerStatus);
        expect(result.error).toBe("The date time format is not correct");
    })

    it("Create an air conditioner status with expected temperature lower than 0 degree", async () => {
        const newAirConditionerStatus: AirConditionerStatus = {
            Air_Conditioner_Id: "0001",
            Current_Temperature: 23,
            Humidity: 74,
            DateTime: "21/12/2018 10:57:45",
            Expected_Temperature: -10.5
        };
        const result: Result = await createAirConditionerStatus(newAirConditionerStatus);
        expect(result.error).toBe("The expected temperature is lower than 0 degree");
    })

    it("Create an air conditioner status but the id is not found", async () => {
        const newAirConditionerStatus: AirConditionerStatus = {
            Air_Conditioner_Id: "0003",
            Current_Temperature: 23,
            Humidity: 74,
            DateTime: "21/12/2018 10:57:45",
            Expected_Temperature: 20.0
        };
        const result: Result = await createAirConditionerStatus(newAirConditionerStatus);
        expect(result.error).toBe("Air Conditioner Id cannot found");
    })

})

describe("Get the Array Of Air Conditioner Status", async () => {
    beforeEach(async () => {
        //make new air conditioner status value
        const newAirConditionerStatus: AirConditionerStatus = {
            Air_Conditioner_Id: "0001",
            Current_Temperature: 23,
            Humidity: 74,
            DateTime: "21/12/2018 10:00:00",
            Expected_Temperature: 20.0
        };
        const newAirConditionerStatus2: AirConditionerStatus = {
            Air_Conditioner_Id: "0001",
            Current_Temperature: 24,
            Humidity: 71,
            DateTime: "21/12/2018 10:15:00",
            Expected_Temperature: 22.0
        };
        const newAirConditionerStatus3: AirConditionerStatus = {
            Air_Conditioner_Id: "0001",
            Current_Temperature: 23,
            Humidity: 77,
            DateTime: "21/12/2018 10:30:00",
            Expected_Temperature: 21.5
        };
        await createAirConditionerStatus(newAirConditionerStatus);
        await createAirConditionerStatus(newAirConditionerStatus2);
        await createAirConditionerStatus(newAirConditionerStatus3);
    })

    it("Get the air conditioner status by id", async () => {
        const result: Result = await getAirConditionerStatus("0001");
        expect(result.airCondtionerStatus).toMatchObject([
            {
                Current_Temperature: 23,
                Humidity: 74,
                DateTime: `2018-12-21 10:00:00`,
                Expected_Temperature: 20.0
            },
            {
                Current_Temperature: 24,
                Humidity: 71,
                DateTime: `2018-12-21 10:15:00`,
                Expected_Temperature: 22.0
            },
            {
                Current_Temperature: 23,
                Humidity: 77,
                DateTime: `2018-12-21 10:30:00`,
                Expected_Temperature: 21.5
            }
        ])
    })

    it("Get the air conditioner status by empty id", async () => {
        const result: Result = await getAirConditionerStatus("");
        expect(result.error).toBe("The air conditioner id cannot be empty");
    })

    it("Get the air conditioner status by incorrect format id", async () => {
        const result: Result = await getAirConditionerStatus("0aa002");
        expect(result.error).toBe("The air conditioner id format is not correct");
    })

    it("Get the air conditioner status by wrong id", async () => {
        const result: Result = await getAirConditionerStatus("0003");
        expect(result.error).toBe("Air Conditioner not found");
    })
})

describe("Get Both Air Conditioner And Air Conditioner Status Of One Air Conditioner Id", async () => {
    beforeEach(async () => {
        //make new air conditioner status value
        const newAirConditionerStatus: AirConditionerStatus = {
            Air_Conditioner_Id: "0001",
            Current_Temperature: 23,
            Humidity: 74,
            DateTime: "21/12/2018 10:00:00",
            Expected_Temperature: 20.0
        };
        const newAirConditionerStatus2: AirConditionerStatus = {
            Air_Conditioner_Id: "0001",
            Current_Temperature: 24,
            Humidity: 71,
            DateTime: "21/12/2018 10:15:00",
            Expected_Temperature: 22.0
        };
        const newAirConditionerStatus3: AirConditionerStatus = {
            Air_Conditioner_Id: "0001",
            Current_Temperature: 23,
            Humidity: 77,
            DateTime: "21/12/2018 10:30:00",
            Expected_Temperature: 21.5
        };
        await createAirConditionerStatus(newAirConditionerStatus);
        await createAirConditionerStatus(newAirConditionerStatus2);
        await createAirConditionerStatus(newAirConditionerStatus3);
    })

    it("Get the information by id", async () => {
        const result: Result = await getAllAirConditionerDataById("0001");
        expect(result).toMatchObject({
            result: true,
            getAirConditioner: {
                Air_Conditioner_Id: "0001",
                Model_No: "AC01",
                Location: "Sad Building"
            },
            airCondtionerStatus: [
                {
                    Current_Temperature: 23,
                    Humidity: 74,
                    DateTime: `2018-12-21 10:00:00`,
                    Expected_Temperature: 20.0
                },
                {
                    Current_Temperature: 24,
                    Humidity: 71,
                    DateTime: `2018-12-21 10:15:00`,
                    Expected_Temperature: 22.0
                },
                {
                    Current_Temperature: 23,
                    Humidity: 77,
                    DateTime: `2018-12-21 10:30:00`,
                    Expected_Temperature: 21.5
                }
            ]
        }
        )
    })

    it("Get the information by empty id", async () => {
        const result: Result = await getAllAirConditionerDataById("");
        expect(result.error).toBe("The air conditioner id cannot be empty");
    })

    it("Get the information by incorrect format id", async () => {
        const result: Result = await getAllAirConditionerDataById("0aa002");
        expect(result.error).toBe("The air conditioner id format is not correct");
    })

    it("Get the information by wrong id", async () => {
        const result: Result = await getAllAirConditionerDataById("0003");
        expect(result.error).toBe("Air Conditioner not found");
    })
})

describe("Get All Both Air Conditioner And Air Conditioner Status Of One Client", async () => {
    beforeEach(async () => {
        //make new air conditioner status value
        const newAirConditionerStatus: AirConditionerStatus = {
            Air_Conditioner_Id: "0001",
            Current_Temperature: 23,
            Humidity: 74,
            DateTime: "21/12/2018 10:00:00",
            Expected_Temperature: 20.0
        };
        const newAirConditionerStatus2: AirConditionerStatus = {
            Air_Conditioner_Id: "0001",
            Current_Temperature: 24,
            Humidity: 71,
            DateTime: "21/12/2018 10:15:00",
            Expected_Temperature: 22.0
        };
        const newAirConditionerStatus3: AirConditionerStatus = {
            Air_Conditioner_Id: "0002",
            Current_Temperature: 23,
            Humidity: 77,
            DateTime: "21/12/2018 10:30:00",
            Expected_Temperature: 21.5
        };
        const newAirConditionerStatus4: AirConditionerStatus = {
            Air_Conditioner_Id: "0002",
            Current_Temperature: 21,
            Humidity: 70,
            DateTime: "21/12/2018 10:45:00",
            Expected_Temperature: 23.0
        };
        await createAirConditionerStatus(newAirConditionerStatus);
        await createAirConditionerStatus(newAirConditionerStatus2);
        await createAirConditionerStatus(newAirConditionerStatus3);
        await createAirConditionerStatus(newAirConditionerStatus4);
    })

    it("Get the information by id", async () => {
        const result: Result = await getAllAirConditionerDataByClient("Hello Company");
        expect(result).toMatchObject({
            result: true,
            airConditionerAllDataArray: [{

                airConditioner: {
                    Air_Conditioner_Id: "0001",
                    Model_No: "AC01",
                    Location: "Sad Building"
                },

                airCondtionerStatus: [
                    {
                        Current_Temperature: 23,
                        Humidity: 74,
                        DateTime: `2018-12-21 10:00:00`,
                        Expected_Temperature: 20.0
                    },
                    {
                        Current_Temperature: 24,
                        Humidity: 71,
                        DateTime: `2018-12-21 10:15:00`,
                        Expected_Temperature: 22.0
                    }
                ]
            }, {

                airConditioner: {
                    Air_Conditioner_Id: "0002",
                    Model_No: "AC02",
                    Location: "Happy Building"
                },

                airCondtionerStatus: [
                    {
                        Current_Temperature: 23,
                        Humidity: 77,
                        DateTime: `2018-12-21 10:30:00`,
                        Expected_Temperature: 21.5
                    },
                    {
                        Current_Temperature: 21,
                        Humidity: 70,
                        DateTime: `2018-12-21 10:45:00`,
                        Expected_Temperature: 23.0
                    }
                ]
            }]
        })
    })

    it("Get the information by empty client name", async () => {
        const result: Result = await getAllAirConditionerDataByClient("");
        expect(result.error).toBe("Client name is empty");
    })

    it("Get the information by wrong client name", async () => {
        const result: Result = await getAllAirConditionerDataByClient("Youtube Company");
        expect(result.error).toBe("Client not found");
    })

})

afterAll(async () => {
    await knex.destroy();
})