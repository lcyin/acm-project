import { createUser } from "../service/user-service";
import { createClient } from "../service/client-service"
import { knex } from "../knex-config";
import { AirConditioner, UpdateAirConditioner, User, Client, Result } from "../interface";
import {
    createAirConditioner, findAirConditionerById,
    updateAirConditioner, getAirConditioner
} from "../service/air_conditioner-service";

let userid: number;
let clientid: number;
let userid2: number;
let clientid2: number;

beforeEach(async () => {

    //drop all table value
    await knex("Air_Conditioner_Status").del();
    await knex("Air_Conditioner").del();
    await knex("User").del();
    await knex("Client").del();

    //make new user value
    const newUser: User = { Username: "hello", Password: "world" };
    await createUser(newUser);
    const user: User[] = await knex.select("*").from("User").where("Username", "hello");
    if(user[0].id){
        userid = user[0].id;
    }

    //make new client value
    const newClient: Client = {
        Name: "Hello Company",
        Telephone: "12345678",
        Email: "1234@gmail.com",
        Address: "Happy Street"
    };
    await createClient(newClient);
    const client: Client[] = await knex.select("*").from("Client").where("Name", "Hello Company");
    if(client[0].id){
        clientid = client[0].id;
    }
})

describe("Create Air Conditioner", () => {

    it("Create a normal air conditioner", async () => {
        const newAirConditioner: AirConditioner = {
            Air_Conditioner_Id: "0001",
            Model_No: "AC01",
            User_Id: userid,
            Client_Id: clientid,
            Location: "Sad Building"
        };
        const result: Result = await createAirConditioner(newAirConditioner);
        const resultAirConditioner: AirConditioner[] = await knex.select("*").from("Air_Conditioner")
            .where("Air_Conditioner_Id", "0001");
        expect(result.result).toBeTruthy();
        expect(resultAirConditioner[0]).toMatchObject({
            id: resultAirConditioner[0].id,
            Air_Conditioner_Id: "0001",
            Model_No: "AC01",
            User_Id: userid,
            Client_Id: clientid,
            Location: "Sad Building"
        })
    })

    it("Create an air conditioner with empty value", async () => {
        const newAirConditioner: AirConditioner = {
            Air_Conditioner_Id: "",
            Model_No: "",
            User_Id: userid,
            Client_Id: clientid,
            Location: ""
        };
        const result: Result = await createAirConditioner(newAirConditioner);
        expect(result.error).toBe("Air conditioner value cannot be empty");
    })

    it("Create an air conditioner with incorrect air conditioner id", async () => {
        const newAirConditioner: AirConditioner = {
            Air_Conditioner_Id: "ABCD",
            Model_No: "AC01",
            User_Id: userid,
            Client_Id: clientid,
            Location: "Sad Building"
        };
        const result: Result = await createAirConditioner(newAirConditioner);
        expect(result.error).toBe("The air conditioner id format is not correct");
    })

    it("Create an air conditioner with non exist user id", async () => {
        const newAirConditioner: AirConditioner = {
            Air_Conditioner_Id: "0001",
            Model_No: "AC01",
            User_Id: userid - 10,
            Client_Id: clientid,
            Location: "Sad Building"
        };
        const result: Result = await createAirConditioner(newAirConditioner);
        expect(result.error).toBe("User id does not found");
    })

    it("Create an air conditioner with non exist client id", async () => {
        const newAirConditioner: AirConditioner = {
            Air_Conditioner_Id: "0001",
            Model_No: "AC01",
            User_Id: userid,
            Client_Id: clientid - 10,
            Location: "Sad Building"
        };
        const result: Result = await createAirConditioner(newAirConditioner);
        expect(result.error).toBe("Client id does not found");
    })

    it("Create an air conditioner that is already existed", async () => {
        const newAirConditioner: AirConditioner = {
            Air_Conditioner_Id: "0001",
            Model_No: "AC01",
            User_Id: userid,
            Client_Id: clientid,
            Location: "Sad Building"
        };
        await createAirConditioner(newAirConditioner);
        const result: Result = await createAirConditioner(newAirConditioner);
        expect(result.error).toBe("Air Conditioner Id already exist");
    })
})

describe("Find Air Conditioner By Id", () => {

    beforeEach(async () => {

        //make new air conditioner value
        const newAirConditioner: AirConditioner = {
            Air_Conditioner_Id: "0001",
            Model_No: "AC01",
            User_Id: userid,
            Client_Id: clientid,
            Location: "Sad Building"
        };
        await createAirConditioner(newAirConditioner);

    })

    it("Find the 0001 air conditioner successfully by id", async () => {
        const result: Result = await findAirConditionerById("0001");
        expect(result.result).toBeTruthy();
        if (result.airconditioner) {
            expect(result.airconditioner).toMatchObject({
                id: result.airconditioner.id,
                Air_Conditioner_Id: "0001",
                Model_No: "AC01",
                User_Id: userid,
                Client_Id: clientid,
                Location: "Sad Building"
            })
        }
    })

    it("Cannot find the 0001 air conditioner with the wrong id", async () => {
        const result: Result = await findAirConditionerById("0002");
        expect(result.result).toBeFalsy();
    })
})

describe("Update The Air Conditioner Information", async () => {

    beforeEach(async () => {

        const newUser2: User = { Username: "happy", Password: "girl" };
        await createUser(newUser2);
        const user2: User[] = await knex.select("*").from("User").where("Username", "happy");
        if(user2[0].id){
            userid2 = user2[0].id;
        }

        const newClient2: Client = {
            Name: "Happy Company",
            Telephone: "24681111",
            Email: "43221@gmail.com",
            Address: "Good Street"
        };
        await createClient(newClient2);
        const client2: Client[] = await knex.select("*").from("Client").where("Name", "Happy Company");
        if(client2[0].id){
            clientid2 = client2[0].id;
        }

        const newAirConditioner: AirConditioner = {
            Air_Conditioner_Id: "0001",
            Model_No: "AC01",
            User_Id: userid,
            Client_Id: clientid,
            Location: "Sad Building"
        };
        const newAirConditioner2: AirConditioner = {
            Air_Conditioner_Id: "0010",
            Model_No: "AC02",
            User_Id: userid,
            Client_Id: clientid,
            Location: "Beautiful Building"
        };
        await createAirConditioner(newAirConditioner);
        await createAirConditioner(newAirConditioner2);
    })

    it("Update only the id", async () => {
        const aupdateAirConditioner: UpdateAirConditioner = {
            OriginalId: "0001",
            UpdatedId: "0002"
        };
        await updateAirConditioner(aupdateAirConditioner);
        const airConditioner: AirConditioner[] = await knex.select("*").from("Air_Conditioner")
            .where("Air_Conditioner_Id", "0002");
        expect(airConditioner[0]).toMatchObject({
            Air_Conditioner_Id: "0002",
            Model_No: "AC01",
            User_Id: userid,
            Client_Id: clientid,
            Location: "Sad Building"
        })
    })

    it("Using the original id after updating", async () => {
        const aupdateAirConditioner: UpdateAirConditioner = {
            OriginalId: "0001",
            UpdatedId: "0002"
        };
        await updateAirConditioner(aupdateAirConditioner);
        const result: Result = await updateAirConditioner(aupdateAirConditioner);
        expect(result.error).toBe("Air Conditioner not found");
    })

    it("Update only the id but the id is already exists", async () => {
        const aupdateAirConditioner: UpdateAirConditioner = {
            OriginalId: "0001",
            UpdatedId: "0010"
        };
        const result: Result = await updateAirConditioner(aupdateAirConditioner);
        expect(result.error).toBe("Update Air Conditioner id already exists");
    })

    it("Update only the model no", async () => {
        const aupdateAirConditioner: UpdateAirConditioner = {
            OriginalId: "0001",
            Model_No: "ABC1"
        };
        await updateAirConditioner(aupdateAirConditioner);
        const airConditioner: AirConditioner[] = await knex.select("*").from("Air_Conditioner")
            .where("Air_Conditioner_Id", "0001");
        expect(airConditioner[0]).toMatchObject({
            Air_Conditioner_Id: "0001",
            Model_No: "ABC1",
            User_Id: userid,
            Client_Id: clientid,
            Location: "Sad Building"
        })
    })

    it("Update only the user id", async () => {
        const aupdateAirConditioner: UpdateAirConditioner = {
            OriginalId: "0001",
            User_Id: userid2
        };
        await updateAirConditioner(aupdateAirConditioner);
        const airConditioner: AirConditioner[] = await knex.select("*").from("Air_Conditioner")
            .where("Air_Conditioner_Id", "0001");
        expect(airConditioner[0]).toMatchObject({
            Air_Conditioner_Id: "0001",
            Model_No: "AC01",
            User_Id: userid2,
            Client_Id: clientid,
            Location: "Sad Building"
        })
    })

    it("Update only the client id", async () => {
        const aupdateAirConditioner: UpdateAirConditioner = {
            OriginalId: "0001",
            Client_Id: clientid2
        };
        await updateAirConditioner(aupdateAirConditioner);
        const airConditioner: AirConditioner[] = await knex.select("*").from("Air_Conditioner")
            .where("Air_Conditioner_Id", "0001");
        expect(airConditioner[0]).toMatchObject({
            Air_Conditioner_Id: "0001",
            Model_No: "AC01",
            User_Id: userid,
            Client_Id: clientid2,
            Location: "Sad Building"
        })
    })

    it("Update only the location", async () => {
        const aupdateAirConditioner: UpdateAirConditioner = {
            OriginalId: "0001",
            Location: "He He Building"
        };
        await updateAirConditioner(aupdateAirConditioner);
        const airConditioner: AirConditioner[] = await knex.select("*").from("Air_Conditioner")
            .where("Air_Conditioner_Id", "0001");
        expect(airConditioner[0]).toMatchObject({
            Air_Conditioner_Id: "0001",
            Model_No: "AC01",
            User_Id: userid,
            Client_Id: clientid,
            Location: "He He Building"
        })
    })

    it("Update all the data", async () => {
        const aupdateAirConditioner: UpdateAirConditioner = {
            OriginalId: "0001",
            UpdatedId: "0002",
            Model_No: "ABC1",
            User_Id: userid2,
            Client_Id: clientid2,
            Location: "He He Building"
        };
        await updateAirConditioner(aupdateAirConditioner);
        const airConditioner: AirConditioner[] = await knex.select("*").from("Air_Conditioner")
            .where("Air_Conditioner_Id", "0002");
        expect(airConditioner[0]).toMatchObject({
            Air_Conditioner_Id: "0002",
            Model_No: "ABC1",
            User_Id: userid2,
            Client_Id: clientid2,
            Location: "He He Building"
        })
    })

    it("Update with empty value", async () => {
        const aupdateAirConditioner: UpdateAirConditioner = {
            OriginalId: "",
            UpdatedId: "",
            Model_No: "",
            User_Id: userid2,
            Client_Id: clientid2,
            Location: ""
        };
        const result: Result = await updateAirConditioner(aupdateAirConditioner);
        expect(result.error).toBe("Air conditioner value cannot be empty");
    })

    it("Update with incorrect air conditioner id", async () => {
        const aupdateAirConditioner: UpdateAirConditioner = {
            OriginalId: "0001",
            UpdatedId: "00a02",
            Model_No: "ABC1",
            User_Id: userid2,
            Client_Id: clientid2,
            Location: "He He Building"
        };
        const result: Result = await updateAirConditioner(aupdateAirConditioner);
        expect(result.error).toBe("The air conditioner id format is not correct");
    })

    it("The update user id not found", async () => {
        const aupdateAirConditioner: UpdateAirConditioner = {
            OriginalId: "0001",
            UpdatedId: "0002",
            Model_No: "ABC1",
            User_Id: userid - 10,
            Client_Id: clientid2,
            Location: "He He Building"
        };
        const result: Result = await updateAirConditioner(aupdateAirConditioner);
        expect(result.error).toBe("User id does not found");
    })

    it("The update client id not found", async () => {
        const aupdateAirConditioner: UpdateAirConditioner = {
            OriginalId: "0001",
            UpdatedId: "0002",
            Model_No: "ABC1",
            User_Id: userid2,
            Client_Id: clientid - 10,
            Location: "He He Building"
        };
        const result: Result = await updateAirConditioner(aupdateAirConditioner);
        expect(result.error).toBe("Client id does not found");
    })
})

describe("Get The Air Conditioner Information", async () => {

    beforeEach(async () => {
        const newAirConditioner: AirConditioner = {
            Air_Conditioner_Id: "0001",
            Model_No: "AC01",
            User_Id: userid,
            Client_Id: clientid,
            Location: "Sad Building"
        };
        await createAirConditioner(newAirConditioner);
    })

    it("Get the air conditioner by id", async () => {
        const result: Result = await getAirConditioner("0001");
        expect(result.getAirConditioner).toMatchObject({
            Air_Conditioner_Id: "0001",
            Model_No: "AC01",
            Location: "Sad Building"
        });
    })


    it("Get the air conditioner with empty id", async () => {
        const result: Result = await getAirConditioner("");
        expect(result.error).toBe("The air conditioner id cannot be empty");
    })

    it("Get the air conditioner with incorrect format id", async () => {
        const result: Result = await getAirConditioner("A0B0");
        expect(result.error).toBe("The air conditioner id format is not correct");
    })

    it("Get the air conditioner with wrong id", async () => {
        const result: Result = await getAirConditioner("0002");
        expect(result.error).toBe("Air Conditioner not found");
    })

})

afterAll(async () => {
    await knex.destroy();
})
