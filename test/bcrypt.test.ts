import { hashPassword, comparePassword } from "../bcrypt";

test("The hash password should not be the same of the original password", async () => {
  const testPassword: string = "abc";
  const ahashPassword: string = await hashPassword(testPassword);
  expect(testPassword == ahashPassword).toBeFalsy();
});

test("Compare hash password with the correct password", async () => {
  const testPassword: string = "abc";
  const ahashPassword: string = await hashPassword(testPassword);
  expect(await comparePassword(testPassword, ahashPassword)).toBeTruthy();
})

test("Compare hash password with the incorrect password", async () => {
  const testPassword: string = "abc";
  const ahashPassword: string = await hashPassword(testPassword);
  expect(await comparePassword("aabbcc", ahashPassword)).toBeFalsy();
})