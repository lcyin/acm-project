import * as dotenv from 'dotenv';

dotenv.config();

module.exports = {

  production: {
    client: "postgresql",
    connection: {
      database: process.env.production_database,
      user: process.env.production_username,
      password: process.env.production_password,
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      directory: __dirname + '/migrations/test'
    }
  },

  test: {
    client: "postgresql",
    connection: {
      database: process.env.test_database,
      user: process.env.test_username,
      password: process.env.test_password,
      host: process.env.test_host
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      directory: __dirname + '/migrations/test'
    }
  }
};