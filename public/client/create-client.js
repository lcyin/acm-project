window.onload =  ()=>{
    const createClientForm = document.getElementById('create-client')

    createClientForm.onsubmit = redirectClient;
};

async function redirectClient(event){
    event.preventDefault();
    const form = this;
    const formData = {};
    for(let input of form){
        if(!['submit','reset'].includes(input.type)){
            formData[input.name] = input.value;
        }
    }
    const id = formData.id;
    const res = await fetch('/client',{
                            'method': 'POST',
                            headers:{
                                "Content-Type":"application/json; charset=utf-8"
                            },
                            body: JSON.stringify(formData)
                        })
    const result = await res.json();
    if(result.result === true){
        alert(formData.name + " created!")

        document.location.href="client.html"
    }else {
        alert(result.error)
    }
}
