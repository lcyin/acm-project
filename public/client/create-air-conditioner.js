window.onload = ()=>{
    const createAirCondtionerForm = document.getElementById('air-conditioner-create')
    createAirCondtionerForm.onsubmit = createAirConditioner;
};

async function createAirConditioner(event){
    event.preventDefault();

    const form = this;
    const formData = {};
    const query = location.search.substring(1);
    clientName = query.replace('%20', ' ');
    formData["clientName"] = clientName;
    for (const input of form) {
        if(!['submit','reset','button'].includes(input.type)){
            if(input.name=="alocation"){
                formData["location"] = input.value;
            }
            else{
                formData[input.name] = input.value;
            }
        }
    }
    console.log(formData);
    const res = await fetch('/air-conditioner',{
        'method': 'POST',
        headers:{
            "Content-Type":"application/json; charset=utf-8"
        },
        body: JSON.stringify(formData)
    })
    const result1 = await res.json()

    if(result1.result===true){
        alert('New Air Contioner Created!');
        document.location.href="client.html"
    }else {
        alert(result1.error);
    }
}