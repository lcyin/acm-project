window.onload = () => {
    //Get the air-con info of the selected client
    const query = location.search.substring(1)
    const client = query.replace('%20', ' ')
    populateClient(client);
    getAirConditionerDataByClient(client);
};

function populateClient(client){
    const clientHeading = document.getElementById('client')
    const clientHTML = `
    <h1>Client: ${client}</h1>
    `;
    clientHeading.innerHTML=clientHTML
}

function populateContainer(airConditioners) {
    const chartElement = document.getElementById('chart');
    let chartHTML = "";
    let airConHTML;
    for (let index = 0; index < airConditioners.length; index++) {
        airConHTML = displayAirConInfo(airConditioners[index].airConditioner.Air_Conditioner_Id, 
            airConditioners[index].airConditioner.Model_No, airConditioners[index].airConditioner.Location);
        chartHTML +=  
            `<div class="chart-row">
                <div class="chart-container" id=${airConditioners[index].airConditioner.Air_Conditioner_Id}>
                    <canvas class="air-conditioner-chart" id="myChart${index + 1}" width="500" height="150">
                    </canvas>
                </div>
                ${airConHTML}
            </div>`;
    }
    chartElement.innerHTML = chartHTML;
}

function populateChart(index, airConData) {
    let ctx = document.getElementById(`myChart${index}`).getContext('2d');
    //Use the data to draw the chart
    let data = {
        labels: airConData.datetime,
        datasets: [{
            label: "Expected Temperature",
            yAxisID: 'A',
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255, 255, 255, 0)",
            borderColor: "blue",
            borderDash: [],
            borderJoinStyle: 'miter',
            pointBorderColor: "black",
            pointBackgroundColor: "white",
            pointBorderWidth: 1,
            pointHoverRadius: 8,
            pointRadius: 4,
            pointHitRadius: 10,
            data: airConData.expected,
        }, {
            label: "Current Temperature",
            yAxisID: 'A',
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255, 255, 255, 0)",
            borderColor: "red",
            borderDash: [],
            pointBorderColor: "blue",
            pointBackgroundColor: "white",
            pointBorderWidth: 1,
            pointHoverRadius: 8,
            pointRadius: 4,
            pointHitRadius: 10,
            data: airConData.current
        }, {
            label: "Humidity",
            yAxisID: 'B',
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255, 255, 255, 0)",
            borderColor: "green",
            borderDash: [],
            pointBorderColor: "orange",
            pointBackgroundColor: "white",
            pointBorderWidth: 1,
            pointHoverRadius: 8,
            pointRadius: 4,
            pointHitRadius: 10,
            data: airConData.humidity
        }]
    };

    let options = {
        scales: {
            yAxes: [{
                id: 'A',
                position: 'left',
                ticks: {
                    suggestedMin: 10,
                    suggestedMax: 30,
                    fontSize: 15,
                    fontColor: 'rgba(0, 0, 0, 1)'
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Temperature',
                    fontSize: 18,
                    fontColor: 'rgba(0, 0, 0, 1)'
                },

            }, {
                id: 'B',
                position: 'right',
                ticks: {
                    suggestedMin: 50,
                    suggestedMax: 100,
                    fontSize: 15,
                    fontColor: 'rgba(0, 0, 0, 1)'
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Humidity',
                    fontSize: 18,
                    fontColor: 'rgba(0, 0, 0, 1)'
                }
            }],
            xAxes: [{
                ticks: {
                    fontColor: 'rgba(0, 0, 0, 1)',
                    fontSize: 15
                },
                scaleLabel: {
                    fontColor: 'rgba(0, 0, 0, 1)'
                }
            }]
        },
        legend: {
            labels: {
                fontColor: 'rgba(0, 0, 0, 1)',
                fontSize: 15
            }
        }

    };

    Chart.defaults.global.defaultFontFamily = "'Raleway', 'sans-serif'";
    new Chart(ctx, {
        type: 'line',
        data: data,
        options: options
    });

}



function displayAirConInfo(airConId, modelNo, location) {
    return `
        <div class="airConInfo">    
            <p>Air-Conditioner No: <br><span>${airConId}</span></p>
            <p>Model No: <br><span>${modelNo}</span></p>
            <p>Location: <br><span>${location}</span></p>  
        </div>
  
    `
}


async function getAirConditionerDataByClient(client) {

    const url = `/air-conditioner-status/getall?name=${client}`;
    const res = await fetch(url);
    const result = await res.json();
    const airConditionerAllDataArray = result.airConditionerAllDataArray;

    populateContainer(airConditionerAllDataArray)

    for (let x = 0; x < airConditionerAllDataArray.length; x++) {
        const airConditionerData = airConditionerAllDataArray[x];
        console.log(airConditionerData);

        //Get the data of the air-con
        if (result.airConditionerAllDataArray.length > 0) {
            // const airCondiitoner = airConditionerData.airConditioner;
            const airCondiitonerStatusArray = airConditionerData.airCondtionerStatus;

            const airConData = {
                expected: [],
                current: [],
                humidity: [],
                datetime: []
            };

            for (let i = 0; i < airCondiitonerStatusArray.length; i++) {
                airConData.expected.push(airCondiitonerStatusArray[i].Expected_Temperature);
                airConData.current.push(airCondiitonerStatusArray[i].Current_Temperature);
                airConData.humidity.push(airCondiitonerStatusArray[i].Humidity);
                airConData.datetime.push(airCondiitonerStatusArray[i].DateTime);
            };

            populateChart(x + 1, airConData)

        } else {
            alert('Air-Conditioner not found!')
        }
    }
}

//Refresh the website every 2 mins
setTimeout(function () {
    location.reload();
}, 120000);