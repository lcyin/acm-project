window.onload = () => {
    const updateClientForm = document.getElementById('update-client')
    updateClientForm.onsubmit = updateClient;
};

async function updateClient() {
    event.preventDefault();
    const form = this;
    const formData = {};

    const query = location.search.substring(1);
    originalname = query.replace('%20', ' ');
    formData["originalname"] = originalname;

    for (let input of form) {
        if (!['submit', 'reset', 'button'].includes(input.type)&&input.value!="") {
            formData[input.name] = input.value;
        }
    }

    if(!formData["updatedname"]&&!formData["telephone"]&&!formData["email"]&&!formData["address"]){
        alert("You do not input any data");
        return;
    }

    const res = await fetch('/client', {
        'method': 'PUT',
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify(formData)
    })
    const result = await res.json();
    if (result.result === true) {
        (formData.updatedname)?
            alert(formData.updatedname + " Update Success!"):
            alert(formData.originalname + " Update Success!")

        document.location.href = "client.html"
    } else {
        alert(result.error)
    }

}

