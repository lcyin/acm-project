window.onload = () => {
    const signupForm = document.getElementById('user-update')

    signupForm.onsubmit = updateUser;
};

async function updateUser(event) {
    event.preventDefault();
    const form = this;
    const formData = {};
    for (let input of form) {
        if (!['submit', 'reset', 'button'].includes(input.type)) {
            formData[input.name] = input.value;
        }
    }
    if (formData.newPassword === formData.confirmPassword) {
        const res = await fetch('/user', {
            'method': 'PUT',
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify(formData)
        })
        const result = await res.json();
        if (result.result === true) {
            alert("Password Changed!")
            document.location.href = "client.html"
        } else {
            alert(result.error)
        }
    } 
    else {
        alert('Confirm Password Not Match');
    }

}
