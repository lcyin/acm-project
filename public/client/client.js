window.onload =  ()=>{
    populateUser()
    popoluateClient();
    const logoutButton = document.getElementById('user-logout')
    const selectClientForm = document.getElementById('get-client')
    const createAirConditionerButton = document.getElementById('air-conditioner-create');
    const clientUpdateButton = document.getElementById("client-update");
    logoutButton.onclick = userLogout;
    createAirConditionerButton.onclick = createAirConditioner;
    clientUpdateButton.onclick = updateAirConditioner;
    selectClientForm.onsubmit = getAllAirCondiitoner;
};

function populateUser(){
    const query = location.search.substring(1)
    const client = query.replace('%20', ' ')
    const userTitle = `
    <h1>Welcome ${client}</h1>
    `;
    document.getElementById('user-title').innerHTML = userTitle;
}

function ClientList(clients){
    return `
    <select name="name" class="form-control">
        ${
            clients.map((client)=>{return Client(client.Name, client.id)}).join('')
        }
    </select>
    `
}

function Client(name, id){
    return `
    <option id="${id}">${name}</option>
    `;
}

async function popoluateClient(){
    const res = await fetch('/client/get-name');
    const clients = await res.json();
    const clientList = document.getElementById('client');
    clientList.innerHTML = ClientList(clients);
    document.querySelector("select.form-control").style.marginTop = "15px";
    document.querySelector("select.form-control").style.marginBottom = "15px";
}

async function userLogout(event){
    event.preventDefault();
    const res = await fetch('/login/logout',{
        'method': 'POST',
    })
    const body = await res.json();
    if(body.result){
        location.href="index.html"
    }
}

function getAllAirCondiitoner(){
    event.preventDefault();
    const form = this;
    const formData = {};
    for(let input of form){
        if(!['submit','reset'].includes(input.type)){

            formData[input.name] = input.value;
        }
    }
    document.location.href=`air-conditioner.html?${formData.name}`
}

function createAirConditioner(){
    const selectClientForm = document.getElementById('get-client')
    const clientName = selectClientForm.name.value
    document.location.href=`create-air-conditioner.html?${clientName}`
}

function updateAirConditioner(){
    const selectClientForm = document.getElementById('get-client')
    const clientName = selectClientForm.name.value
    document.location.href=`update-client.html?${clientName}`
}