window.onload = () => {
    const loginForm = document.getElementById('user-login')

    loginForm.onsubmit = redirectClient;
};

async function redirectClient(event) {
    event.preventDefault();
    const form = this;
    const formData = {};
    for (let input of form) {
        if (!['submit', 'reset'].includes(input.type)) {
            formData[input.name] = input.value;
        }
    }
    const res = await fetch('/login', {
        'method': 'POST',
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify(formData)
    })
    const result = await res.json();
    if (result.result === true) {
        alert("Welcome " + formData.username)

        document.location.href = `client.html?${formData.username}`
    } else {
        alert(result.error)
    }
}
