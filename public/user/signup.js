window.onload =  ()=>{
    const signupForm = document.getElementById('user-signup')

    signupForm.onsubmit = redirectLogin;
};

async function redirectLogin(event){
    event.preventDefault();
    const form = this;
    const formData = {};
    for(let input of form){
        if(!['submit','reset'].includes(input.type)){
            formData[input.name] = input.value;
        }
    }
    const id = formData.id;
    const res = await fetch('/user',{
                            'method': 'POST',
                            headers:{
                                "Content-Type":"application/json; charset=utf-8"
                            },
                            body: JSON.stringify(formData)
                        })
    const result = await res.json();
    if(result.result === true){
        alert("Sign Up Success!")
        document.location.href="index.html"
    }else {
        alert(result.error)
    }
}
