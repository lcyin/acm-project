import * as Knex from "knex";

exports.up = async (knex: Knex): Promise<void> => {
    const hasTable: boolean = await knex.schema.hasTable("User");
    if (!hasTable) {
        await knex.schema.createTable("User", table => {
            table.increments();
            table.text("Username").unique().notNullable();
            table.text("Password").notNullable();
        })
    }
};

exports.down = async (knex: Knex): Promise<void> => {
    const hasTable: boolean = await knex.schema.hasTable("User");
    if (hasTable) {
        await knex.schema.dropTable("User");
    }
};
