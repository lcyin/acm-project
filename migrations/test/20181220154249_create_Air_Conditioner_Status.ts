import * as Knex from "knex";

exports.up = async (knex: Knex): Promise<void> => {
    const hasTable: boolean = await knex.schema.hasTable("Air_Conditioner_Status");
    if (!hasTable) {
        await knex.schema.createTable("Air_Conditioner_Status", table => {
            table.increments();
            table.integer("Air_Conditioner_Id").unsigned().notNullable();
            table.float("Current_Temperature").unsigned().notNullable();
            table.float("Expected_Temperature").unsigned().notNullable();
            table.float("Humidity").unsigned().notNullable();
            table.timestamp("DateTime").notNullable();
            table.foreign("Air_Conditioner_Id").references("Air_Conditioner.id");
        })
    }
};

exports.down = async (knex: Knex): Promise<void> => {
    const hasTable: boolean = await knex.schema.hasTable("Air_Conditioner_Status");
    if (hasTable) {
        await knex.schema.dropTable("Air_Conditioner_Status");
    }
};
