import * as Knex from "knex";

exports.up = async (knex: Knex): Promise<void> => {
    const hasTable: boolean = await knex.schema.hasTable("Air_Conditioner");
    if (!hasTable) {
        await knex.schema.createTable("Air_Conditioner", table => {
            table.increments();
            table.text("Air_Conditioner_Id").unique().notNullable();
            table.text("Model_No").notNullable();
            table.integer("User_Id").unsigned().notNullable();
            table.integer("Client_Id").unsigned().notNullable();
            table.text("Location").notNullable();
            table.foreign("User_Id").references("User.id");
            table.foreign("Client_Id").references("Client.id");
        })
    }
};

exports.down = async (knex: Knex): Promise<void> => {
    const hasTable: boolean = await knex.schema.hasTable("Air_Conditioner");
    if (hasTable) {
        await knex.schema.dropTable("Air_Conditioner");
    }
};
