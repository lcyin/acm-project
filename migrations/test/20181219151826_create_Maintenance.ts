import * as Knex from "knex";

exports.up = async (knex: Knex): Promise<void> => {
    const hasTable: boolean = await knex.schema.hasTable("Maintenance");
    if (!hasTable) {
        await knex.schema.createTable("Maintenance", table => {
            table.increments();
            table.integer("Air_Conditioner_Id").unsigned().notNullable();
            table.date("Maintenance_Date").notNullable();
            table.integer("Follow_By_Id").unsigned().notNullable();
            table.text("Status").notNullable();
            table.foreign("Air_Conditioner_Id").references("Air_Conditioner.id");
            table.foreign("Follow_By_Id").references("User.id");
        })
    }
};

exports.down = async (knex: Knex): Promise<void> => {
    const hasTable: boolean = await knex.schema.hasTable("Maintenance");
    if (hasTable) {
        await knex.schema.dropTable("Maintenance");
    }
};
