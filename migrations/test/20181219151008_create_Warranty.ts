import * as Knex from "knex";

exports.up = async (knex: Knex): Promise<void> => {
    const hasTable: boolean = await knex.schema.hasTable("Warranty");
    if (!hasTable) {
        await knex.schema.createTable("Warranty", table => {
            table.increments();
            table.integer("Client_Id").unsigned().notNullable();
            table.integer("Warranty_Id").unsigned().notNullable();
            table.date("Expired_Date").notNullable();
            table.foreign("Client_Id").references("Client.id");
        })
    }
};

exports.down = async (knex: Knex): Promise<void> => {
    const hasTable: boolean = await knex.schema.hasTable("Warranty");
    if (hasTable) {
        await knex.schema.dropTable("Warranty");
    }
};
