import * as Knex from "knex";

exports.up = async (knex: Knex): Promise<void> => {
    const hasTable: boolean = await knex.schema.hasTable("Client");
    if (!hasTable) {
        await knex.schema.createTable("Client", table => {
            table.increments();
            table.text("Name").unique().notNullable();
            table.text("Telephone").notNullable();
            table.text("Email").notNullable();
            table.text("Address").notNullable();
        })
    }
};

exports.down = async (knex: Knex): Promise<void> => {
    const hasTable: boolean = await knex.schema.hasTable("Client");
    if (hasTable) {
        await knex.schema.dropTable("Client");
    }
};
