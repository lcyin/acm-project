import { knex } from "../knex-config";
import { Result, Client, ClientName, UpdateClient, databaseId } from "../interface";

//create a client if the client name does not exist
export async function createClient(newClient: Client): Promise<Result> {
    try {
        //check if the client value are empty
        const keysArray: string[] = Object.keys(newClient);
        for (let key of keysArray) {
            if (newClient[key] == "") {
                const result: Result = { result: false, error: "Client value cannot be empty" };
                return result;
            }
        }

        //Telephone number and email checking
        if (newClient.Telephone.match(/^[0-9]+$/) == null) {
            const result: Result = { result: false, error: "The telephone number format is not correct" };
            return result;
        }
        if (newClient.Email.match(/^[^@ ]+@[^@ ]+$/) == null) {
            const result: Result = { result: false, error: "The email format is not correct" };
            return result;
        }

        const client: Client[] = await knex.select("*").from("Client").where("Name", newClient.Name);

        //client does not exist
        if (client.length == 0) {
            await knex.insert({
                Name: newClient.Name,
                Telephone: newClient.Telephone,
                Email: newClient.Email,
                Address: newClient.Address
            }).into("Client");
            const result: Result = { result: true };
            return result;
        }

        //client exists
        else {
            const result: Result = { result: false, error: "Client name already exist" };
            return result;
        }
    }
    catch (err) { throw err; }
}

//use the id primary key to find the client
export async function findClientById(id: number): Promise<Result> {
    try {
        const client: Client[] = await knex.select("*").from("Client").where("id", id);

        //client does not exist
        if (client.length == 0) {
            const result: Result = { result: false, error: "Client not found" };
            return result;
        }

        //client exists
        else {
            const result: Result = { result: true, client: client[0] };
            return result;
        }
    }
    catch (err) { throw err; }
}

//use the name to find the client database id
export async function findIdByName(name: string): Promise<Result> {
    try {
        const client: databaseId[] = await knex.select("id").from("Client").where("Name", name);

        //client does not exist
        if (client.length == 0) {
            const result: Result = { result: false, error: "Client not found" };
            return result;
        }

        //client exists
        else {
            const result: Result = { result: true, id:client[0].id };
            return result;
        }
    }
    catch (err) { throw err; }
}

//get all the client id and name from the database
export async function getAllClientName(): Promise<ClientName[]> {
    try {
        const client: ClientName[] = await knex.select("id","Name").from("Client");
        return client;
    }
    catch (err) { throw err; }
}

//update a client information
export async function updateClient(updateClient: UpdateClient): Promise<Result> {
    try {
        //check if the client value are empty
        const keysArray: string[] = Object.keys(updateClient);
        for (let key of keysArray) {
            if (updateClient[key] == "") {
                const result: Result = { result: false, error: "Client value cannot be empty" };
                return result;
            }
        }

        //Telephone number and email checking
        if (updateClient.Telephone) {
            if (updateClient.Telephone.match(/^[0-9]+$/) == null) {
                const result: Result = { result: false, error: "The telephone number format is not correct" };
                return result;
            }
        }
        if (updateClient.Email) {
            if (updateClient.Email.match(/^[^@ ]+@[^@ ]+$/) == null) {
                const result: Result = { result: false, error: "The email format is not correct" };
                return result;
            }
        }

        //ensure the original client name will be found in the database
        const client: Client[] = await knex.select("*").from("Client")
            .where("Name", updateClient.OriginalName);
        if (client.length == 0) {
            const result: Result = { result: false, error: "Client not found" };
            return result;
        }

        //ensure the update client name will not be found in the database
        if (updateClient.UpdatedName) {
            const client: Client[] = await knex.select("*").from("Client")
                .where("Name", updateClient.UpdatedName);
            //client name found
            if (client.length != 0) {
                const result: Result = { result: false, error: "Update client name already exists" };
                return result;
            }
        }

        //the sql object that put in the knex sql
        let insertSQL = {};
        if (updateClient.UpdatedName) { insertSQL["Name"] = updateClient.UpdatedName; }
        if (updateClient.Telephone) { insertSQL["Telephone"] = updateClient.Telephone; }
        if (updateClient.Email) { insertSQL["Email"] = updateClient.Email; }
        if (updateClient.Address) { insertSQL["Address"] = updateClient.Address; }

        await knex("Client").update(insertSQL).where("Name", updateClient.OriginalName);
        const result: Result = { result: true };
        return result;
    }
    catch (err) { throw err; }
}