import { knex } from "../knex-config";
import { Result, AirConditioner, UpdateAirConditioner, GetAirConditioner } from "../interface";
import { findUserById } from "./user-service";
import { findClientById } from "./client-service";

//create a air conditioner if the air conditioner id does not exist
export async function createAirConditioner(newAirConditioner: AirConditioner): Promise<Result> {
    try {

        //check if the air conditioner value are empty 
        const keysArray: string[] = Object.keys(newAirConditioner);
        for (let key of keysArray) {
            if (typeof newAirConditioner[key] == "string") {
                if (newAirConditioner[key] == "") {
                    const result: Result = { result: false, error: "Air conditioner value cannot be empty" };
                    return result;
                }
            }
        }

        const airconditioner: AirConditioner[] = await knex.select("*").from("Air_Conditioner")
            .where("Air_Conditioner_Id", newAirConditioner.Air_Conditioner_Id);
        //Air conditioner id checking
        if (newAirConditioner.Air_Conditioner_Id.match(/^[0-9]+$/) == null) {
            const result: Result = { result: false, error: "The air conditioner id format is not correct" };
            return result;
        }

        //Air conditioner does not exist
        if (airconditioner.length == 0) {

            //check if the user and client id is exist
            const userResult: Result = await findUserById(newAirConditioner.User_Id);
            if (!userResult.result) {
                const result: Result = { result: false, error: "User id does not found" };
                return result;
            }
            const clientResult: Result = await findClientById(newAirConditioner.Client_Id);
            if (!clientResult.result) {
                const result: Result = { result: false, error: "Client id does not found" };
                return result;
            }

            await knex.insert({
                Air_Conditioner_Id: newAirConditioner.Air_Conditioner_Id,
                Model_No: newAirConditioner.Model_No,
                User_Id: newAirConditioner.User_Id,
                Client_Id: newAirConditioner.Client_Id,
                Location: newAirConditioner.Location
            }).into("Air_Conditioner");
            const result: Result = { result: true };
            return result;
        }

        //Air Conditioner Id exists
        else {
            const result: Result = { result: false, error: "Air Conditioner Id already exist" };
            return result;
        }
    }
    catch (err) { throw err; }
}

//use the id primary key to find the air conditioner
export async function findAirConditionerById(id: string): Promise<Result> {
    try {
        const airconditioner: AirConditioner[] = await knex.select("*").from("Air_Conditioner")
            .where("Air_Conditioner_Id", id);

        //air conditioner does not exist
        if (airconditioner.length == 0) {
            const result: Result = { result: false, error: "Air Conditioner not found" };
            return result;
        }

        //air conditioner exists
        else {
            const result: Result = { result: true, airconditioner: airconditioner[0] };
            return result;
        }
    }
    catch (err) { throw err; }
}

//update an air conditioner information
export async function updateAirConditioner(updateAirConditioner: UpdateAirConditioner): Promise<Result> {
    try {
        //check if the air conditioner value are empty
        const keysArray: string[] = Object.keys(updateAirConditioner);
        for (let key of keysArray) {
            if (typeof updateAirConditioner[key] == "string") {
                if (updateAirConditioner[key] == "") {
                    const result: Result = { result: false, error: "Air conditioner value cannot be empty" };
                    return result;
                }
            }
        }

        //Air conditioner id and expected temperature checking
        if (updateAirConditioner.UpdatedId) {
            if (updateAirConditioner.UpdatedId.match(/^[0-9]+$/) == null) {
                const result: Result = { result: false, error: "The air conditioner id format is not correct" };
                return result;
            }
        }

        //ensure the original air conditioner id will be found in the database
        const airconditioner: AirConditioner[] = await knex.select("*").from("Air_Conditioner")
            .where("Air_Conditioner_Id", updateAirConditioner.OriginalId);
        if (airconditioner.length == 0) {
            const result: Result = { result: false, error: "Air Conditioner not found" };
            return result;
        }

        //ensure the update air conditioner id will not be found in the database
        if (updateAirConditioner.UpdatedId) {
            const airconditioner: AirConditioner[] = await knex.select("*").from("Air_Conditioner")
                .where("Air_Conditioner_Id", updateAirConditioner.UpdatedId);
            //air conditioner id found
            if (airconditioner.length != 0) {
                const result: Result = { result: false, error: "Update Air Conditioner id already exists" };
                return result;
            }
        }

        //check if the user and client id is exist
        if (updateAirConditioner.User_Id) {
            const userResult: Result = await findUserById(updateAirConditioner.User_Id);
            if (!userResult.result) {
                const result: Result = { result: false, error: "User id does not found" };
                return result;
            }
        }
        if (updateAirConditioner.Client_Id) {
            const clientResult: Result = await findClientById(updateAirConditioner.Client_Id);
            if (!clientResult.result) {
                const result: Result = { result: false, error: "Client id does not found" };
                return result;
            }
        }

        //the sql object that put in the knex sql
        let insertSQL = {};
        if (updateAirConditioner.UpdatedId) { insertSQL["Air_Conditioner_Id"] = updateAirConditioner.UpdatedId; }
        if (updateAirConditioner.Model_No) { insertSQL["Model_No"] = updateAirConditioner.Model_No; }
        if (updateAirConditioner.User_Id) { insertSQL["User_Id"] = updateAirConditioner.User_Id; }
        if (updateAirConditioner.Client_Id) { insertSQL["Client_Id"] = updateAirConditioner.Client_Id; }
        if (updateAirConditioner.Location) { insertSQL["Location"] = updateAirConditioner.Location; }

        await knex("Air_Conditioner").update(insertSQL)
            .where("Air_Conditioner_Id", updateAirConditioner.OriginalId);
        const result: Result = { result: true };
        return result;
    }
    catch (err) { throw err; }
}

//get the model no and location of the air conditioner id
export async function getAirConditioner(airConditionerId: string): Promise<Result> {
    try {
        //id cannot be empty
        if (airConditionerId == "") {
            const result: Result = { result: false, error: "The air conditioner id cannot be empty" };
            return result;
        }
        //id can only contain no
        if (airConditionerId.match(/^[0-9]+$/) == null) {
            const result: Result = { result: false, error: "The air conditioner id format is not correct" };
            return result;
        }

        //ensure the original air conditioner id will be found in the database
        const airconditioner: GetAirConditioner[] = await knex
            .select("Air_Conditioner_Id", "Model_No", "Location").from("Air_Conditioner")
            .where("Air_Conditioner_Id", airConditionerId);
        if (airconditioner.length == 0) {
            const result: Result = { result: false, error: "Air Conditioner not found" };
            return result;
        }
        else {
            const result: Result = {
                result: true,
                getAirConditioner: airconditioner[0]
            }
            return result;
        }
    }
    catch (err) { throw err; }
}



