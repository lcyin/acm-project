import { knex } from "../knex-config";
import {
    Result, AirConditionerStatus, GetAirConditionerStatus, databaseId,
    AirConditionerId, AirConditionerAllData
} from "../interface";
import { findAirConditionerById, getAirConditioner } from "./air_conditioner-service"

//change the date format in create function
export function createDateFormatChange(date: string) {
    const year = date.substring(6, 10);
    const month = date.substring(3, 5);
    const day = date.substring(0, 2);
    const time = date.substring(11);
    return `${year}-${month}-${day} ${time}`;
}

//change the date format in get function
export function getDateFormatChange(date: Date) {
    const year = date.getFullYear();
    const month = (date.getMonth() + 1 < 10) ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1);
    const day = (date.getDate() < 10) ? "0" + date.getDate() : date.getDate();
    const hour = (date.getHours() < 10) ? "0" + date.getHours() : date.getHours();
    const minute = (date.getMinutes() < 10) ? "0" + date.getMinutes() : date.getMinutes();
    const second = (date.getSeconds() < 10) ? "0" + date.getSeconds() : date.getSeconds();
    return `${year}-${month}-${day} ${hour}:${minute}:${second}`;
}

//create a air conditioner status 
export async function createAirConditionerStatus(newAirConditionerStatus: AirConditionerStatus): Promise<Result> {
    try {

        const aresult: Result = await findAirConditionerById(newAirConditionerStatus.Air_Conditioner_Id);
        //detector data checking
        if (newAirConditionerStatus.Current_Temperature < 0) {
            const result: Result = { result: false, error: "The current temperature is lower than 0 degree" };
            return result;
        }
        if (newAirConditionerStatus.Humidity < 0 || newAirConditionerStatus.Humidity > 100) {
            const result: Result = { result: false, error: "The humidity should be 0-100" };
            return result;
        }
        if (newAirConditionerStatus.DateTime
            .match(/^[0-9]{2}\/[0-9]{2}\/[0-9]{4} [0-9]{2}:[0-9]{2}:[0-9]{2}$/) == null) {
            const result: Result = { result: false, error: "The date time format is not correct" };
            return result;
        }
        if (newAirConditionerStatus.Expected_Temperature < 0) {
            const result: Result = { result: false, error: "The expected temperature is lower than 0 degree" };
            return result;
        }

        //Air conditioner id exists
        if (aresult.result && aresult.airconditioner) {
            let dateTime: string = createDateFormatChange(newAirConditionerStatus.DateTime);
            await knex.insert({
                Air_Conditioner_Id: aresult.airconditioner.id,
                Current_Temperature: newAirConditionerStatus.Current_Temperature,
                Expected_Temperature: newAirConditionerStatus.Expected_Temperature,
                Humidity: newAirConditionerStatus.Humidity,
                DateTime: dateTime
            }).into("Air_Conditioner_Status");
            const result: Result = { result: true };
            return result;
        }

        //Air Conditioner Id does not exist
        else {
            const result: Result = { result: false, error: "Air Conditioner Id cannot found" };
            return result;
        }
    }
    catch (err) { throw err; }
}

//get all the information of the air conditioner id
export async function getAirConditionerStatus(airConditionerId: string): Promise<Result> {
    try {

        //id cannot be empty
        if (airConditionerId == "") {
            const result: Result = { result: false, error: "The air conditioner id cannot be empty" };
            return result;
        }
        //id can only contain no
        if (airConditionerId.match(/^[0-9]+$/) == null) {
            const result: Result = { result: false, error: "The air conditioner id format is not correct" };
            return result;
        }
        //change the air conditioner id to the index id
        const airConditioner = await knex.select("id").from("Air_Conditioner")
            .where("Air_Conditioner_Id", airConditionerId);
        //id not found
        if (airConditioner.length == 0) {
            const result: Result = { result: false, error: "Air Conditioner not found" };
            return result;
        }
        //id found
        else {
            const anairConditionerStatus: GetAirConditionerStatus[] = await knex
                .select("Current_Temperature", "Expected_Temperature", "Humidity", "DateTime")
                .from("Air_Conditioner_Status")
                .where("Air_Conditioner_Id", airConditioner[0].id);
            for (let eachStatus of anairConditionerStatus) {
                eachStatus.DateTime = getDateFormatChange(new Date(eachStatus.DateTime));
            }
            const result: Result = { result: true, airCondtionerStatus: anairConditionerStatus }
            return result;
        }

    }
    catch (err) { throw err; }
}

//get the both air conditioner and the status data of the air conditioner id
export async function getAllAirConditionerDataById(airConditionerId: string): Promise<Result> {
    try {
        const result1: Result = await getAirConditioner(airConditionerId);
        if (result1.result) {
            const result2: Result = await getAirConditionerStatus(airConditionerId);
            if (result2.result) {
                const result3: Result = {
                    result: true, getAirConditioner: result1.getAirConditioner,
                    airCondtionerStatus: result2.airCondtionerStatus
                }
                return result3;
            }
            //something wrong when getting the air conditioner status data
            else {
                return result2;
            }
        }
        //something wrong when getting the air conditioner data
        else {
            return result1;
        }
    }
    catch (err) { return err; }
}

//get the all the both air conditioner and the status data of the target client name
export async function getAllAirConditionerDataByClient(name: string): Promise<Result> {
    try {
        //cannot have empty client name
        if (name == "") {
            const result: Result = { result: false, error: "Client name is empty" };
            return result;
        }
        //convert the name to the client id
        const client: databaseId[] = await knex.select("id").from("Client").where("Name", name);
        //cannot find the client name
        if (client.length == 0) {
            const result: Result = { result: false, error: "Client not found" };
            return result;
        }
        //find the client name
        else {
            const id: number = client[0].id;
            //get all the air conditioner id of the target client
            const allAirConditionerId: AirConditionerId[] = await knex.select("Air_Conditioner_Id")
                .from("Air_Conditioner")
                .where("Client_Id", id);
            let airConditionerArray: AirConditionerAllData[] = new Array();
            let result1: Result;
            let allData: AirConditionerAllData;
            //group all the air conditioner information into an array
            for (let eachId of allAirConditionerId) {
                result1 = await getAllAirConditionerDataById(eachId.Air_Conditioner_Id);
                if (result1.result && result1.getAirConditioner && result1.airCondtionerStatus) {
                    allData = {
                        airConditioner: result1.getAirConditioner,
                        airCondtionerStatus: result1.airCondtionerStatus
                    }
                    airConditionerArray.push(allData);
                }
                //something goes wrong and cannot get the information
                else {
                    return result1;
                }
            }
            const result2: Result = { result: true, airConditionerAllDataArray: airConditionerArray };
            return result2;
        }
    }
    catch (err) { return err; }
}