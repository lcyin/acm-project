import { knex } from "../knex-config";
import { hashPassword, comparePassword } from "../bcrypt";
import { User, Result } from "../interface";

//create a user if the username does not exist
export async function createUser(newUser: User): Promise<Result> {
    try {
        //check if the username or password are empty
        const keysArray: string[] = Object.keys(newUser);
        for (let key of keysArray) {
            if (newUser[key] == "") {
                const result: Result = { result: false, error: "Username or password cannot be empty" };
                return result;
            }
        }

        const ahashPassword: string = await hashPassword(newUser.Password);
        const user: User[] = await knex.select("*").from("User").where("Username", newUser.Username);

        //username does not exist
        if (user.length == 0) {
            await knex.insert({ Username: newUser.Username, Password: ahashPassword }).into("User");
            const result: Result = { result: true };
            return result;
        }

        //username exists
        else {
            const result: Result = { result: false, error: "Username already exist" };
            return result;
        }
    }
    catch (err) { throw err; }
}

//check the username and password is correct
export async function authentication(loginUser: User): Promise<Result> {
    try {
        //check if the username or password are empty
        const keysArray: string[] = Object.keys(loginUser);
        for (let key of keysArray) {
            if (loginUser[key] == "") {
                const result: Result = { result: false, error: "Username or password cannot be empty" };
                return result;
            }
        }

        const user: User[] = await knex.select("*").from("User").where("Username", loginUser.Username);

        //username does not exist
        if (user.length == 0) {
            const result: Result = { result: false, error: "Username is not correct" };
            return result;
        }

        //username exists
        else {
            const isPasswordCorrect: boolean = await comparePassword(loginUser.Password, user[0].Password);
            //correct password
            if (isPasswordCorrect) {
                const result: Result = { result: true, loginUser: user[0] };
                return result;
            }
            //incorrect password
            else {
                const result: Result = { result: false, error: "Password is not correct" };
                return result;
            }
        }
    }
    catch (err) { throw err; }
}

//use the id primary key to find the user
export async function findUserById(id: number): Promise<Result> {
    try {
        const user: User[] = await knex.select("*").from("User").where("id", id);

        //user does not exist
        if (user.length == 0) {
            const result: Result = { result: false, error: "User not found" };
            return result;
        }

        //user exists
        else {
            const result: Result = { result: true, loginUser: user[0] };
            return result;
        }
    }
    catch (err) { throw err; }
}

export async function changePassword(updateUser: User): Promise<Result> {
    try {
        //check if the username or password are empty
        const keysArray: string[] = Object.keys(updateUser);
        for (let key of keysArray) {
            if (updateUser[key] == "") {
                const result: Result = { result: false, error: "Username or password cannot be empty" };

                return result;
            }
        }

        const ahashPassword: string = await hashPassword(updateUser.Password);
        const user: User[] = await knex.select("*").from("User").where("Username", updateUser.Username);

        //username exists
        if (user.length != 0) {
            await knex("User").update({"Password":ahashPassword}).where("Username", updateUser.Username);
            const result: Result = { result: true };
            return result;
        }

        //username does not exists
        else {
            const result: Result = { result: false, error: "Username does not exist" };

            return result;
        }
    }
    catch (err) { throw err; }
}


