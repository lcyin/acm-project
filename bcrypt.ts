import * as bcrypt from "bcryptjs";

export async function hashPassword(password: string): Promise<string> {
    const salt: string = await bcrypt.genSalt();
    const hashPassword: string = await bcrypt.hash(password, salt);
    return hashPassword;
}

export async function comparePassword(inputPassword: string, hashPassword: string) {
    const isCorrect: boolean = await bcrypt.compare(inputPassword, hashPassword);
    return isCorrect;
}
