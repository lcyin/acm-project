const rpiDhtSensor = require('rpi-dht-sensor');
let axios = require('axios');

let dht = new rpiDhtSensor.DHT11(4);

function addZero(i) {
    if(i < 10) {
        i = "0" + 1;
    }
    return i;
}

function clockTick() {
    let currentTime = new Date(),
        month = currentTime.getMonth() + 1,
        day = currentTime.getDate(),
        year = currentTime.getFullYear(),
        hours = addZero(currentTime.getHours()),
        minutes = addZero(currentTime.getMinutes()),
        seconds = addZero(currentTime.getSeconds()),
        text = (day + "/" + month + "/" + year + ' ' + hours + ':' + minutes + ':' + seconds);
    // here we get the element with the id of "date" and change the content to the text variable we made above
    return text;
}

(async function read() {
    setTimeout(read, 900000);

    let readout = dht.read();

    let newData = {
        'id': '0001',
        'temperature': readout.temperature,
        'humidity': readout.humidity,
        'timestamp': clockTick()
    };

    await axios.post("http://13.229.129.241//air-conditioner-status", newData);
    console.log(newData);

})();
