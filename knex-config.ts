import * as Knex from "knex";
const knexConfig = require('./knexfile');
export const knex = Knex(knexConfig["test"] || knexConfig["production"]);
