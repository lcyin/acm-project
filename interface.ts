export interface User {
    id?:number,
    Username: string,
    Password: string
}

export interface Result {
    result: boolean,
    loginUser?: User,
    client?: Client,
    airconditioner?: AirConditioner,
    getAirConditioner?: GetAirConditioner,
    airCondtionerStatus?: GetAirConditionerStatus[],
    airConditionerAllData?: AirConditionerAllData,
    airConditionerAllDataArray?: AirConditionerAllData[],
    id?: number,
    error?: string
}

export interface Client {
    id?:number,
    Name: string,
    Telephone: string,
    Email: string,
    Address: string,
}

export interface ClientName {
    id: number
    Name: string
}

export interface UpdateClient {
    id?:number,
    OriginalName: string
    UpdatedName?: string,
    Telephone?: string,
    Email?: string,
    Address?: string,
}

export interface AirConditioner {
    id?:number,
    Air_Conditioner_Id: string,
    Model_No: string,
    User_Id: number,
    Client_Id: number,
    Location: string
}

export interface UpdateAirConditioner {
    id?:number,
    OriginalId: string,
    UpdatedId?: string,
    Model_No?: string,
    User_Id?: number,
    Client_Id?: number,
    Location?: string
}

export interface GetAirConditioner {
    Air_Conditioner_Id: string,
    Model_No: string,
    Location: string
}

export interface AirConditionerId {
    Air_Conditioner_Id: string,
}

export interface AirConditionerStatus {
    id?:number,
    Air_Conditioner_Id: string,
    Current_Temperature: number,
    Humidity: number,
    DateTime: string,
    Expected_Temperature: number
}

export interface GetAirConditionerStatus {
    Current_Temperature: number,
    Expected_Temperature: number
    Humidity: number,
    DateTime: string
}

export interface AirConditionerAllData {
    airConditioner: GetAirConditioner,
    airCondtionerStatus: GetAirConditionerStatus[],    
}

export interface databaseId {
    id:number
}
